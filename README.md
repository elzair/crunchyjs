crunchy.js
==========

This is my humble attempt to create a kick-ass frontend framework.

API
---

### alloc(obj, num)

Allocates an array of `num` objects that are equivalent to `obj`

#### Example

```javascript
const crunchy = require('crunhy');

const obj = {name: 'Mack', age: 52};

const objArr = crunchy.alloc(obj, 100);
```
