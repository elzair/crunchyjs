"use strict";

const p = require('protolib');

const alloc = exports.alloc = function(obj, num) {
  const arr = [];
  
  for (let i = 0; i < num; i++) {
    arr[i] = p.clone(obj);
  }

  return arr;
};
